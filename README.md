# Code Challenge - Backend

Para la realización del proyectos se ha utilizado Laradock.
Para ejucutar el proyecto no es necesario realizar una instalación en local de PHP, MySQL, ...

Solo necesitariamos lo siguiente para poder ejecutarlo en local:
- Git
- Docker
- Docker-Compose

La configuración de los contenedores está dentro de la carpeta de Laradock.

El comando para lanzar los contenedores que necesitamos es:

`docker-compose up -d nginx mysql phpmyadmin`

Se ejecutara desde la consola situandos sobre la carpeta Laradock del proyecto.

Para ejecutar los comandos necesarios para finalizar la instalación del proyecto. Tales como composer, artisan, ... podremos ejecutar:

- `docker-compose exec workspace bash` (para entrar en contenerdor que contiene el proyecto de laravel)
- `docker-compose exec mysql bash` (para entrar en el contenedor de mysql)

Habría que tener en cuenta la configuración que se quiera realizar de bbdd, password, url
