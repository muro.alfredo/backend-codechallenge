<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = ['direccion_id','telefono'];
    protected $hidden = ['created_at', 'updated_at'];

    public function direccion()
    {
        return $this->hasOne('App\Direccion', 'id');
    }

    public function users()
    {
        return $this->morphMany('App\User', 'user');
    }
}
