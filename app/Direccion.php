<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Direccion extends Model
{
    protected $fillable = ['tipo','nombre','numero','cp'];
    protected $hidden = ['created_at', 'updated_at'];
}
