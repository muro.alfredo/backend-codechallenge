<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $fillable = ['matricula','coche'];
    protected $hidden = ['created_at', 'updated_at'];

    public function users()
    {
        return $this->morphMany('App\User', 'user');
    }
}
