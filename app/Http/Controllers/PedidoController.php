<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Direccion;
use App\Driver;
use App\Http\Resources\PedidoResource;
use App\Pedido;
use App\Repositories\Contracts\PedidoRepository;
use App\Services\PedidoService;
use App\User;
use Illuminate\Http\Request;

class PedidoController extends Controller
{
    /**
     * @var PedidoService
     */
    protected $pedidoService;
    protected $pedidoRepository;

    /**
     * PedidoController constructor.
     * @param PedidoService $pedidoService
     */
    public function __construct(
        PedidoService $pedidoService,
        PedidoRepository $pedidoRepository
    )
    {
        $this->pedidoService = $pedidoService;
        $this->pedidoRepository = $pedidoRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $email = $request->email;

        $cliente = User::where('email',$email)->count();

        if($cliente > 0){
            return response()->json(['error' => 'El email ya existe en base de datos.'], 403);
        }

        $franjaCorrecta = $this->pedidoService->validarFranjaHoras($request);

        if(!$franjaCorrecta){
            return response()->json(
                ['error' => 'La diferencia de las horas del intervalo de entrega tiene que ser mayor que 1 y menor que 8.'],
                403
            );
        }

        $direccion = $this->pedidoRepository->crearDireccion($request);
        /** @var Cliente $cliente */
        $cliente = $this->pedidoRepository->crearCliente($request,$direccion);
        $this->pedidoRepository->crearUserCliente($request,$cliente);
        $driver = $this->pedidoRepository->getDriver();
        $hoy = new \DateTime('now');

        $pedido = Pedido::create([
            'fechaEntrega'      =>  $hoy->modify('+7 day'),
            'franjaHoraInicio'  =>  $request->franjaHoraInicio,
            'franjaHoraFin'     =>  $request->franjaHoraFin,
            'cliente_id'        =>  $cliente->id,
            'driver_id'         =>  $driver->id
        ]);

        return response()->json($pedido, 201);
    }

    public function getPedidosDriver(Driver $driver, $fecha){
        $fechaInicioBusqueda = new \DateTime($fecha.' 00:00:00');
        $fechaFinBusqueda = new \DateTime($fecha.' 23:59:59');

        $pedidos = Pedido::where('driver_id',$driver->id)
            ->where('fechaEntrega','>=',$fechaInicioBusqueda)
            ->where('fechaEntrega','<=',$fechaFinBusqueda)
            ->get();

        $infoCompletaPedido = $this->pedidoService->pedidosCompletos($pedidos);

        return response()->json($infoCompletaPedido, 200);
    }
}
