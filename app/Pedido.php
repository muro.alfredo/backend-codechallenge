<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    protected $fillable = ['fechaEntrega', 'franjaHoraInicio', 'franjaHoraFin','cliente_id','driver_id'];
    protected $hidden = ['created_at', 'updated_at'];

    public function cliente()
    {
        return $this->hasOne('App\Cliente', 'id');
    }

    public function driver()
    {
        return $this->hasOne('App\Driver', 'id');
    }
}
