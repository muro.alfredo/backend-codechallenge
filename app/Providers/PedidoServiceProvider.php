<?php

namespace App\Providers;

use App\Repositories\Eloquent\EloquentPedidoRepository;
use App\Services\PedidoService;
use Illuminate\Support\ServiceProvider;

class PedidoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Services\PedidoService', function ($app) {
            return new PedidoService();
        });

        $this->app->bind('App\Repositories\Contracts\PedidoRepository', 'App\Repositories\Eloquent\EloquentPedidoRepository');
    }
}
