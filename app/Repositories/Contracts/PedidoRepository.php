<?php

namespace App\Repositories\Contracts;

interface PedidoRepository
{
    public function crearDireccion($request);

    public function crearCliente($request,$telefono);

    public function getDriver();

    public function crearUserCliente($request,$client);
}
