<?php

namespace App\Repositories\Eloquent;

use App\Cliente;
use App\Direccion;
use App\Driver;
use App\Pedido;
use App\Repositories\Contracts\PedidoRepository;

use App\User;
use Kurt\Repoist\Repositories\Eloquent\AbstractRepository;

class EloquentPedidoRepository extends AbstractRepository implements PedidoRepository
{
    public function entity()
    {
        return Pedido::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function crearDireccion($request){

        $tipo = $request->tipo;
        $nombre = $request->nombre;
        $numero = $request->numero;
        $cp = $request->cp;

        $direccion = Direccion::create([
            'tipo'  =>  $tipo,
            'nombre'=>  $nombre,
            'numero'=>  $numero,
            'cp'    =>  $cp
        ]);

        return $direccion;
    }

    /**
     * @param $request
     * @param $telefono
     * @return mixed
     */
    public function crearCliente($request,$direccion){
        $telefono = $request->telefono;

        $cliente = Cliente::create([
            'telefono'      =>  $telefono,
            'direccion_id'  =>  $direccion->id
        ]);

        return $cliente;
    }

    public function crearUserCliente($request,$cliente){

        $userCliente = User::create([
            'name'      =>  $request->name,
            'lastname'  =>  $request->lastname,
            'email'     =>  $request->email,
            'user_type' =>  Cliente::class,
            'user_id'   =>  $cliente->id
        ]);

        return $userCliente;
    }

    /**
     * @return mixed
     */
    public function getDriver(){
        $driver = Driver::inRandomOrder()->first();
        return $driver;
    }
}
