<?php

namespace App\Services;

use App\Pedido;
use App\User;

class PedidoService
{
    /**
     * @param $request
     * @return bool
     */
    public function validarFranjaHoras($request){
        $franjaHoraInicio = new \DateTime($request->franjaHoraInicio);
        $franjaHoraFin = new \DateTime($request->franjaHoraFin);
        $dateInterval = $franjaHoraInicio->diff($franjaHoraFin);
        $horas = intval($dateInterval->format('%H'));

        if($horas < 1 || $horas > 8){
            return false;
        }

        return true;
    }

    /**
     * @param $pedidos
     * @return array
     */
    public function pedidosCompletos($pedidos){

        $info = array();
        /** @var Pedido $pedido */
        foreach ($pedidos as $pedido) {
            $user = User::where('user_id',$pedido->cliente->id)->first();
            $direccion = $pedido->cliente->direccion;
            $info[] = [
                'Nombre completo cliente'   =>  $user->name.' '.$user->lastname,
                'Email cliente'             =>  $user->email,
                'Telefono cliente'          =>  $pedido->cliente->telefono,
                'Direccion'                 =>  $direccion->tipo.' '.$direccion->nombre.' '.$direccion->numero.', '.$direccion->cp,
                'Franja Horaria Entrega'    =>  'De '.$pedido->franjaHoraInicio.' a '.$pedido->franjaHoraFin,
            ];
        }

        return $info;
    }
}
