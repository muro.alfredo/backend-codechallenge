<?php

use Faker\Generator as Faker;
use Faker\Provider\Address;

$factory->define(App\Direccion::class, function (Faker $faker) {
    $arrayValues = ['Avenida','Calle','Urbanización'];
    return [
        'tipo'  =>  $arrayValues[rand(0,2)],
        'nombre'=>  $faker->address,
        'numero'=>  $faker->numberBetween(1,100),
        'cp'    =>  Address::postcode()
    ];
});
