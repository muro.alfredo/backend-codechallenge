<?php

use Faker\Generator as Faker;

$factory->define(App\Driver::class, function (Faker $faker) {
    return [
        'matricula' =>  $faker->hexColor,
        'coche'     =>  $faker->colorName
    ];
});
