<?php

use Faker\Generator as Faker;

$factory->define(App\Pedido::class, function (Faker $faker) {

    $hoy = new \DateTime('now');

    return [
        'fechaEntrega'      =>  $hoy->modify('+7 day'),
        'franjaHoraInicio'  =>  $faker->time(),
        'franjaHoraFin'     =>  $faker->time()
    ];
});
