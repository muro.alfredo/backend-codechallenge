<?php

use Illuminate\Database\Seeder;
use App\Cliente;
use App\User;
use App\Direccion;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Direccion::class, 5)->create()->each(function($direccion){
            factory(Cliente::class)->create(['direccion_id'=>$direccion->id])->each(function(Cliente $cliente){
                $cliente->users()->save(factory(User::class)->create());
            });
        });

//        factory(Cliente::class, 5)->create()->each(function(Cliente $cliente){
//            $cliente->users()->save(factory(User::class)->create());
//        });
    }
}
