<?php

use Illuminate\Database\Seeder;
use App\Driver;
use App\User;

class DriversTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Driver::class, 5)->create()->each(function(Driver $driver){
            $driver->users()->save(factory(User::class)->create());
        });
    }
}
