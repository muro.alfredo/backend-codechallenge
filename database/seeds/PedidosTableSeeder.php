<?php

use Illuminate\Database\Seeder;
use App\Pedido;
use App\Cliente;
use App\Driver;

class PedidosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Pedido::class)->create();

        $pedido = Pedido::where('cliente_id',null)->first();

        $clientes = Cliente::all();
        $drivers = Driver::all();
        $pedido->cliente_id = $clientes[0]->id;
        $pedido->driver_id = $drivers[0]->id;
        $pedido->save();
    }
}
