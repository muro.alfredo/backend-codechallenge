<?php

use Illuminate\Http\Request;
use App\Pedido;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('v1/pedidos', 'PedidoController@store')->name('pedidos.store');

Route::get('v1/driver/{driver}/fecha/{fecha}/pedidos', 'PedidoController@getPedidosDriver')->name('pedidos.driver.fecha');
