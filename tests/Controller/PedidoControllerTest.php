<?php

namespace Tests\Controller;

use App\Cliente;
use App\Driver;
use App\Pedido;
use Tests\TestCase;

class PedidoControllerTest extends TestCase
{

    public function testNuevoYFalloEmailStore()
    {
        $payload = [
            'name'          =>  'Test',
            'lastname'      =>  'Surname Test',
            'email'         =>  'email@test.com',
            'telefono'      =>  '600000000',
            'tipo'          =>  'Calle',
            'nombre'        =>  'Los Olivos',
            'numero'        =>  '9',
            'cp'            =>  '45280',
            'franjaHoraInicio'=>'10:00',
            'franjaHoraFin' =>  '18:00'
        ];

        $this->json('POST', '/api/v1/pedidos', $payload)
            ->assertStatus(201);

        $this->json('POST', '/api/v1/pedidos', $payload)
            ->assertStatus(403)
            ->assertJson([
                'error' => 'El email ya existe en base de datos.'
            ]);
    }

    public function testFalloFranjaHorariaStore()
    {
        $payload = [
            'name'          =>  'Test',
            'lastname'      =>  'Surname Test',
            'email'         =>  'email@test.com',
            'telefono'      =>  '600000000',
            'tipo'          =>  'Calle',
            'nombre'        =>  'Los Olivos',
            'numero'        =>  '9',
            'cp'            =>  '45280',
            'franjaHoraInicio'=>'10:00',
            'franjaHoraFin' =>  '23:00'
        ];

        $this->json('POST', '/api/v1/pedidos', $payload)
            ->assertStatus(403)
            ->assertJson([
                'error' => 'La diferencia de las horas del intervalo de entrega tiene que ser mayor que 1 y menor que 8.'
            ]);
    }

    public function testgetPedidosDriver(){

        factory(Pedido::class)->create();
        factory(Cliente::class)->create();
        factory(Driver::class)->create();
        $pedido = Pedido::where('cliente_id',null)->first();

        $clientes = Cliente::all();
        $drivers = Driver::all();
        $pedido->cliente_id = $clientes[0]->id;
        $pedido->driver_id = $drivers[0]->id;
        $pedido->save();

        $fecha = new \DateTime('now');

        $this->get(route('pedidos.driver.fecha', ['driver'=>$drivers[0]->id,'fecha'=>$fecha->format('Ymd')]))
            ->assertStatus(200);
    }
}
